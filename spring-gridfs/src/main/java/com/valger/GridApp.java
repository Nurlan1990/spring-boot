package com.valger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GridApp {

    public static void main(String[] args) {
        SpringApplication.run(GridApp.class, args);
    }

}
