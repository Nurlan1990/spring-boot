package com.valger.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.valger.model.Address;

@Repository
public interface AddressRepository extends CrudRepository<Address,Long> {
}
