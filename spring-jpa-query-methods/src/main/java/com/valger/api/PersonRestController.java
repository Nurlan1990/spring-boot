package com.valger.api;


import com.valger.model.NameCount;
import com.valger.repo.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import com.valger.model.Address;
import com.valger.model.Person;
import com.valger.repo.PersonRepository;
import com.valger.service.PersonService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/rest/person")
public class PersonRestController {
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private PersonService personService;


    @GetMapping("/")
    public List<Person> getPersonList() {
        List<Person> personList = new ArrayList<>();
        personRepository.findAll().forEach(personList::add);
        return personList;
    }

    @GetMapping("/search")
    public List<Person> search(
            @RequestParam(name = "name") String name,
            @RequestParam(name = "surname") String surname
    ) {
        return personRepository.
                findPersonByNameLikeOrSurnameLikeOrderByNameDesc("%"+name+"%", "%"+surname+"%");
    }

    @GetMapping("/page")
    public Page<Person> getPersonPage(
            @RequestParam(name = "page",required = false, defaultValue = "0") int page,
            @RequestParam(name = "size",required = false, defaultValue = "5") int size,
            @RequestParam(name = "sort",required = false, defaultValue = "person_id") String sortColumn,
            @RequestParam(name = "dir",required = false, defaultValue = "asc") String sortDirection,
            @RequestParam(name = "filter",required = false, defaultValue = "") String filter
    ) {
        PageRequest pageRequest = PageRequest.of(page,size, Sort.Direction.fromString(sortDirection),sortColumn);
        Page<Person> personPage = personRepository.findAll(pageRequest, "%"+filter+"%");
        System.out.println("total elements = "+personPage.getTotalElements());
        System.out.println("total page = "+personPage.getTotalPages());
        System.out.println("number of elements = "+personPage.getNumberOfElements());
        System.out.println("page size = "+personPage.getSize());
        System.out.println("sort = "+personPage.getSort());

        return personPage;
    }

    @GetMapping("/name-count")
    public List<NameCount> getNameStatistics() {
        return personRepository.getNameStatistics();
    }


    @GetMapping("/{id}")
    public Person getPersonById(@PathVariable(name = "id") long id) {
        Optional<Person> optionalPerson = personRepository.findById(id);
        if(optionalPerson.isPresent()) {
            return optionalPerson.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Person with id " + id + " not found!");
        }
    }

    @PostMapping("/")
    public Person addPerson(@RequestBody Person person) {
        return personService.addPerson(person);
    }

    @PutMapping("/{id}")
    @Transactional
    public Person savePerson(@PathVariable(name = "id") long person_id,
                             @RequestBody Person person) {

        saveAddress(person.getAddress().getId(),person.getAddress());

        return personRepository.save(person);
    }

    @PutMapping("/address/{address_id}")
    public Address saveAddress(@PathVariable(name = "address_id") long address_id,
                               @RequestBody Address address) {
        return addressRepository.save(address);
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Transactional
    public void deletePerson(@PathVariable(name = "id") long person_id) {
        if(personRepository.existsById(person_id)) {
            long address_id = getPersonById(person_id).getAddress().getId();
            personRepository.deleteById(person_id);
            addressRepository.deleteById(address_id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Person with id " + person_id + " not found!");
        }
    }





}
