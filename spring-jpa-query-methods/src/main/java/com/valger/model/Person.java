package com.valger.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name ="person_table")
public class Person implements Serializable {


    private static final long serialVersionUID = -2260028869238400461L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="person_id")
    private long id;

    @Column(name ="first_name",length = 50,nullable = false)
    private String name;

    @Column(name ="last_name", length = 50, nullable = false)
    private String surname;

    @OneToOne
    @JoinColumn(name ="adrress_id", referencedColumnName = "id")
    private Address address;

    public Person() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", address=" + address +
                '}';
    }
}
