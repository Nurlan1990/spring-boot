package com.valger.model;

public interface NameCount {
    String getName();
    long getCount();

}
