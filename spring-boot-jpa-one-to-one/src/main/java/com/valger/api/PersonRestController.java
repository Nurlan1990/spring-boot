package com.valger.api;


import com.valger.model.Address;
import com.valger.model.Person;
import com.valger.repo.AddressRepository;
import com.valger.repo.PersonRepository;
import com.valger.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/rest/person")
public class PersonRestController {
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private PersonService personService;


    @GetMapping("/")
    public List<Person> getPersonList() {
        List<Person> personList = new ArrayList<>();
        personRepository.findAll().forEach(personList::add);
        return personList;
    }

    @GetMapping("/search")
    public List<Person> search(
            @RequestParam(name = "name") String name,
            @RequestParam(name = "surname") String surname
    ) {
        return personRepository.findPersonByNameStartingWithAndSurnameStartingWithOrderByIdDesc(name, surname);
    }



    @GetMapping("/{id}")
    public Person getPersonById(@PathVariable(name = "id") long id) {
        Optional<Person> optionalPerson = personRepository.findById(id);
        if(optionalPerson.isPresent()) {
            return optionalPerson.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Person with id " + id + " not found!");
        }
    }

    @PostMapping("/")
    public Person addPerson(@RequestBody Person person) {
        return personService.addPerson(person);
    }

    @PutMapping("/{id}")
    @Transactional
    public Person savePerson(@PathVariable(name = "id") long person_id,
                             @RequestBody Person person) {
        
            saveAddress(person.getAddress().getId(), person.getAddress());
            return personRepository.save(person);


    }

    @PutMapping("/address/{address_id}")
    public Address saveAddress(@PathVariable(name = "address_id") long address_id,
                               @RequestBody Address address) {
        return addressRepository.save(address);
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Transactional
    public void deletePerson(@PathVariable(name = "id") long person_id) {
        if(personRepository.existsById(person_id)) {
            long address_id = getPersonById(person_id).getAddress().getId();
            personRepository.deleteById(person_id);
            addressRepository.deleteById(address_id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Person with id " + person_id + " not found!");
        }
    }





}
