package com.valger.service;

import com.valger.model.Address;
import com.valger.model.Person;
import com.valger.repo.AddressRepository;
import com.valger.repo.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonService {
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Transactional
    public Person addPerson(Person person) {
        Address address = addressRepository.save(person.getAddress());
        person.setAddress(address);
        return personRepository.save(person);
    }






}
