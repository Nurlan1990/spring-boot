package com.valger.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "address_table")
public class Address implements Serializable {

    private static final long serialVersionUID = -6312646983417959650L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String info;

    // relationship owner side - Person
    @JsonIgnore
    @OneToOne(mappedBy = "address")
    private Person person;


    public Address() {
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", info='" + info + '\'' +
                ", person=" + person +
                ", person=" + person +
                '}';
    }
}
