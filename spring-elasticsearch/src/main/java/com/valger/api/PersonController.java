package com.valger.api;

import com.valger.entity.Person;
import com.valger.repository.PersonRepository;;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.Calendar;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/person")
public class PersonController {


    private final PersonRepository personRepository;

    @PostConstruct
    public void init(){
        Person person = new Person();
        person.setName("valger");
        person.setSurname("the best");
        person.setAdress("ukrain");
        person.setId("K0001");
        personRepository.save(person);
    }

    @GetMapping("/{search}")
    public ResponseEntity<List<Person>> getPerson(@PathVariable String search) {
        List<Person> people = personRepository.findByNameLikeOrSurnameLike(search, search);
        return ResponseEntity.ok(people);
    }
}
