package com.valger.service;

import com.valger.model.Author;
import com.valger.model.Book;
import com.valger.repo.AuthorRepository;
import com.valger.repo.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private AuthorRepository authorRepository;


    public List<Book> getBookList() {
        return bookRepository.findAll();
    }


    public Optional<Book> getBookById(long id) {
        return bookRepository.findById(id);
    }


    public List<Author> getBookAuthors(long bookId) {
        return authorRepository.findAuthorsByBookId(bookId);
    }

    @Transactional
    public Book addBook(Book book) {
        System.out.println("before save book = " + book);

        Book bookFromDb = bookRepository.save(book);
        System.out.println("book from db = " + bookFromDb);

        bookFromDb.getAuthors().forEach(author -> {
            author.getBooks().add(bookFromDb);
            authorRepository.save(author);
        });

        return bookFromDb;
    }

}
