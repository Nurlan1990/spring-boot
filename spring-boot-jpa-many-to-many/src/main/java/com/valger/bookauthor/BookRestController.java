package com.valger.bookauthor;

import com.valger.model.Author;
import com.valger.model.Book;
import com.valger.repo.AuthorRepository;
import com.valger.repo.BookRepository;
import com.valger.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/books/")
public class BookRestController {

    @Autowired
    private BookService bookService;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @GetMapping("/")
    public List<Book> getBooks() {
        return bookService.getBookList();
    }

    @GetMapping("/{id}")
    public Book getBookById(@PathVariable(name = "id") long bookId) {
        return bookService.getBookById(bookId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Book " + bookId + " not found!"));
    }

    @GetMapping("/{id}/authors")
    public List<Author> getBookAuthors(@PathVariable(name = "id") long bookId) {
        Book book = bookService.getBookById(bookId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Book " + bookId + " not found!"));

        return bookService.getBookAuthors(bookId);
    }

    @PostMapping("/")
    public Book addBook(@RequestBody Book book) {
        return bookService.addBook(book);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBoook(@PathVariable(name = "id") long id) {
        if(bookRepository.existsById(id)) {
            bookRepository.deleteById(id);
        } else {
            new ResponseStatusException(HttpStatus.NOT_FOUND, "Book " + id + " not found!");
        }
    }

    @GetMapping("/authors")
    public List<Author> getAuthorList() {
        return authorRepository.findAll();
    }

    @GetMapping("/authors/{id}")
    public Author getAuthorById(@PathVariable(name = "id") long authorId) {
        return authorRepository.findById(authorId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Author " + authorId + " not found"));
    }

    @GetMapping("/authors/{id}/books")
    public List<Book> getAuthorBooks(@PathVariable(name = "id") long authorId) {
        Author author = authorRepository.findById(authorId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Author " + authorId + " not found"));

        return bookRepository.findBooksByAuthorId(authorId);
    }

}
