package com.valger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jpa12manyApplication {
    public static void main(String[] args) {
        SpringApplication.run(Jpa12manyApplication.class, args);

    }
}
