package com.valger.repo;

import com.valger.model.ContactInfo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ContactInfoRepository extends CrudRepository<ContactInfo,Long> {



    @Query(value = "select *  " +
            "from contact_info c " +
            "where person_id=:person_id", nativeQuery = true)
    List<ContactInfo> findAllById(@Param("person_id") long person_id);


    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Transactional
    @Query(value = "delete from contact_info where person_id=:person_id", nativeQuery = true)
    void deleteAllById(@Param("person_id") long person_id);
}
