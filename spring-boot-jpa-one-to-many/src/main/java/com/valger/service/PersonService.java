package com.valger.service;

import com.valger.model.Person;
import com.valger.repo.ContactInfoRepository;
import com.valger.repo.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonService {
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ContactInfoRepository contactInfoRepository;

    @Transactional
    public Person addPerson(Person person) {

         person = personRepository.save(person);

        Person finalPerson = person;

        person.getContactInfoList().forEach(contactInfo ->{
            contactInfo.setPerson(finalPerson);
            contactInfoRepository.save(contactInfo);
                });

        return person;
    }






}
