package com.valger.api;


import com.valger.model.ContactInfo;
import com.valger.model.Person;
import com.valger.repo.ContactInfoRepository;
import com.valger.repo.PersonRepository;
import com.valger.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/rest/person")
public class PersonRestController {
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ContactInfoRepository contactInfoRepository;


    @Autowired
    private PersonService personService;




    @GetMapping("/")
    public List<Person> getPersonList() {
        List<Person> personList = new ArrayList<>();
        personRepository.findAll().forEach(personList::add);
        return personList;
    }


    @GetMapping("/{id}")
    public Person getPersonById(@PathVariable(name = "id") long id) {
        Optional<Person> optionalPerson = personRepository.findById(id);
        if(optionalPerson.isPresent()) {
            return optionalPerson.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Person with id " + id + " not found!");
        }
    }

    @PostMapping("/")
    public Person addPerson(@RequestBody Person person) {
        return personService.addPerson(person);
    }

    @PutMapping("/{id}")
    @Transactional
    public Person savePerson(@PathVariable(name = "id") long person_id,
                             @RequestBody Person person) {

        return personRepository.save(person);
    }



    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Transactional
    public void deletePerson(@PathVariable(name = "id") long person_id) {
        if(personRepository.existsById(person_id)) {

            contactInfoRepository.deleteAllById(person_id);
            personRepository.deleteById(person_id);

        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Person with id " + person_id + " not found!");
        }
    }





}
