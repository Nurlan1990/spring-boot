package com.valger.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "person_table")
public class Person implements Serializable {


    private static final long serialVersionUID = 8022059778327967124L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="person_id")
    private long id;

    @Column(name ="first_name",length = 50,nullable = false)
    private String name;

    @Column(name ="last_name", length = 50, nullable = false)
    private String surname;

    @OneToMany(mappedBy = "person")
    private List<ContactInfo> contactInfoList;

    public Person() {
        this.contactInfoList = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<ContactInfo> getContactInfoList() {
        return contactInfoList;
    }

    public void setContactInfoList(List<ContactInfo> contactInfoList) {
        this.contactInfoList = contactInfoList;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", contactInfoList=" + contactInfoList +
                '}';
    }
}
